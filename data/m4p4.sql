DROP DATABASE IF EXISTS m3p4;
CREATE DATABASE m3p4;
USE m3p4;

CREATE TABLE marcadores(
  id int NOT NULL AUTO_INCREMENT,
  enlace varchar(255),
  etiqueta varchar(255),
  descripcion varchar(255),
  larga text,
  tipo varchar(255),
  PRIMARY KEY(id)
  );

INSERT INTO marcadores(enlace,etiqueta,descripcion,larga,tipo) VALUES
 ('https://www.google.es/','Google','Buscador de Internet','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. ','publicos'),
('https://www.netflix.com/es/','Netflix','Series online','�De d�nde viene?Al contrario del pensamiento popular, el texto de Lorem Ipsum no es simplemente texto aleatorio.','publicos'),
('https://www.bibsonomy.org/','Bibsonomy','Servicio de marcadores sociales','Lorem Ipsum ha sido el texto de relleno est�ndar de las industrias desde el a�o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us� una galer�a de textos y los mezcl� de tal manera que logr� hacer un libro de textos especimen. No s�lo sobrevivi� 500 a�os, sino que tambien ingres� como texto de relleno en documentos electr�nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci�n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m�s recientemente con software de autoedici�n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.','privados')
  ;
