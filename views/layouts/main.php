<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->name,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Públicos', 'url' => ['/marcadores/publicos']],
                    ['label' => 'Privados', 'url' => ['/marcadores/privados']],
                    ['label' => 'Admin', 'url' => ['/marcadores/admin']],
                    
                ],
            ]);
            NavBar::end();
            ?>
            
<div class="menu">
            <ul class="nav nav-pills nav-justified">
                <li><?= Html::a('Publicos', ['marcadores/publicos',], ['class' => 'profile-link']) ?></li>
                <li><?= Html::a('Privados', ['marcadores/privados',], ['class' => 'profile-link']) ?></li>
                <li><?= Html::a('Admin', ['marcadores/admin',], ['class' => 'profile-link']) ?></li>
            </ul>
        </div> 
        

        <div class="container">
<?=
Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
])
?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>


        <footer class="footer">
            <div class="container">
                <p class="pull-left">Cristina Galán <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
