<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;

?>
<?=Html::a('Nuevo', ['marcadores/create'],["class"=>"btn btn-primary","style"=>"margin:5px"]); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>"",
        'columns' => [
            
            [
                'attribute' => 'id',
                'label' => 'ID ',
                'format' => 'raw',
                'value' => function ($model) {
                     return Html::a($model->id,['marcadores/update',"id"=>$model->id]); // your url here
                },
            ],
            [
                'attribute' => 'enlace',
                'label' => 'Enlace ',
                'format' => 'raw',
                'value' => function ($model) {
                     return Html::a($model->etiqueta, $model->enlace,['target'=>'_blank']); // your url here
                },
            ],
            
            [
                'attribute'=>'descripcion',
                'label'=>'descripcion',
                'format'=>'raw',
                'value'=> function($data)
                          { 
                               return  Html::a($data->descripcion, ['marcadores/privados','id'=>$data->id]);      
                          }, 
                
            ],
            
            
        ],
    ]); ?>

<?php
    if($datos["id"]){
            Modal::begin([
                //'id'=>'modal',
                'header'=>"<h2>".$datos['descripcion']."</h2>",
                 'footer'=>'<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a> <a href="'.$datos["enlace"].'" class="btn btn-primary">Visitar</a>',
                'clientOptions'=>[
                    'show'=>TRUE,
                ],
               
                'closeButton' => [

                  'label' => 'X',

                  'class' => 'btn btn-danger btn-sm pull-right',

                ],

                'size' => 'modal-lg',

            ]);

           echo $datos["larga"];

    Modal::end();}

        ?>
<?=Html::a('Nuevo', ['marcadores/create'],["class"=>"btn btn-primary","style"=>"margin:5px"]); ?>