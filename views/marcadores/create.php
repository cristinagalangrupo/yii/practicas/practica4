<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */

$this->title = 'Create Marcadores';
$this->params['breadcrumbs'][] = ['label' => 'Marcadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marcadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
