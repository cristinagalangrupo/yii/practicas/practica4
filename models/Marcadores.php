<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marcadores".
 *
 * @property int $id
 * @property string $enlace
 * @property string $etiqueta
 * @property string $descripcion
 * @property string $larga
 * @property string $tipo
 */
class Marcadores extends \yii\db\ActiveRecord
{
     private $valores=[
      "Publico"=>"Publico",
      "Privado"=>"Privado",
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marcadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['larga'], 'string'],
            ['tipo','in','range'=> array_keys($this->getValores())],
            [['enlace', 'etiqueta', 'descripcion', 'tipo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'enlace' => 'Enlace',
            'etiqueta' => 'Etiqueta',
            'descripcion' => 'Descripcion',
            'larga' => 'Larga',
            'tipo' => 'Tipo',
        ];
    }
    public function getValores(){
        return $this->valores;
    }
}
